<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontController extends Controller
{
    private function convertToArray($stdClassList){
        $jsonData = json_encode($stdClassList);
        return json_decode($jsonData, true);
    }
    
    private function converter($list){
        $list = $this->convertToArray($list);
        for($i=0;$i<count($list);$i++){
            $list[$i] = $this->convertToArray($list[$i]);
        }
        return $list;
    }
    
    public function home(){
        $productList = DB::table("product")->join("media__media", "product.media_id", "=", "media__media.id")->select("product.id", "title", "flag", "product.name", "price_old", "price_new", "media__media.provider_reference")->limit(3)->get();
        $sliderList = DB::table("product")->join("media__media", "product.media_id", "=", "media__media.id")->select("product.id", "title", "flag", "product.name", "price_old", "price_new", "media__media.provider_reference")->limit(6)->get();
        $feedback = DB::table("feedback")->select("full_name", "content", "created_at")->where("is_activated", 1)->get();
        $tag = DB::table("classification__tag")->select("name")->get();
        $article = DB::table("article")->select("id", "name")->whereNotNull("seo_title")->get();
        $content = DB::table("article")->select("name", "content")->where("id", 13)->get();
        
//        convert mảng kết quả từ kiểu stdClass sang kiểu array
        $productList = $this->converter($productList);
        $sliderList = $this->converter($sliderList);
        $feedback = $this->converter($feedback);
        $tag = $this->converter($tag);
        $article = $this->converter($article);
        $content = $this->converter($content);
        return view("partial.home", ["productList"=>$productList, "sliderList"=>$sliderList, "feedback"=>$feedback, "tag"=>$tag, "article"=>$article, "content"=>$content]);
    }
    
    public function hatChia(){
        $productList = DB::table("product")->join("media__media", "product.media_id", "=", "media__media.id")->select("product.id", "title", "flag", "product.name", "price_old", "price_new", "media__media.provider_reference")->where("cate_id", 2)->limit(2)->get();
        $article = DB::table("article")->join("media__media", "article.image_id", "=", "media__media.id")->select("article.id", "article.name", "media__media.provider_reference")->whereNotNull("seo_title")->get();
        
        $productList = $this->converter($productList);
        $article = $this->converter($article);
        return view("partial.hatchia", ["productList"=>$productList, "article"=>$article, "flag"=>"Hạt Chia Úc"]);
    }
    
    public function product($id){
        $product = DB::table("product")->join("media__media", "product.media_id", "=", "media__media.id")->select("product.id", "title", "flag", "product.name", "price_old", "price_new", "media__media.provider_reference")->where("product.id", $id)->get();
        $article = DB::table("article")->select("id", "name")->whereNotNull("seo_title")->get();
        
        $product = $this->converter($product);
        $article = $this->converter($article);
        return view("partial.product", ["id"=>$id, "article"=>$article, "product"=>$product]);
    }
    
    public function order($id){
        $product = DB::table("product")->join("media__media", "product.media_id", "=", "media__media.id")->select("product.id", "title", "flag", "product.name", "price_old", "price_new", "media__media.provider_reference")->where("product.id", $id)->get();
        $article = DB::table("article")->select("id", "name")->whereNotNull("seo_title")->get();
        
        $product = $this->converter($product);
        $article = $this->converter($article);
        return view("partial.order", ["id"=>$id, "article"=>$article, "product"=>$product]);
    }
    
    public function news($id){
        $productList = DB::table("product")->join("media__media", "product.media_id", "=", "media__media.id")->select("product.id", "title", "flag", "product.name", "price_old", "price_new", "media__media.provider_reference")->limit(3)->get();
        $tag = DB::table("classification__tag")->join("article_tag", "article_tag.tag_id", "=", "classification__tag.id")->select("classification__tag.id", "classification__tag.name")->where("article_tag.article_id", $id)->get();
        $article = DB::table("article")->select("name", "content")->where("id", $id)->get();
        
        $productList = $this->converter($productList);
        $tag = $this->converter($tag);
        $article = $this->converter($article);
        return view("partial.tintuc", ["productList"=>$productList, "tag"=>$tag, "article"=>$article]);
    }
}
