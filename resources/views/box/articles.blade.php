<link type="text/css" rel="stylesheet" href="{{asset('css/articles.css')}}"/>

<div class="article">
    @foreach($article as $anArticle)
    <?php
        $name = validate($anArticle["name"]);
        $id = validate($anArticle["id"]);
        $image = validate($anArticle["provider_reference"]);
    ?>
    <div class="an-article">
        <img src="{{asset('images/thumb/'. $image)}}"/>
        <div class="title">
            <a class="green-link" href="/tin-tuc/{{$id}}">{{$name}}</a>
            <p class="black-title">{{$name}}</p>
        </div>
    </div>
    @endforeach
</div>
