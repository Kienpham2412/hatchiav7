<link type="text/css" rel="stylesheet" href="{{asset('css/tag.css')}}"/>
<?php
    $tag;
?>

<div class="tag-box">
    <span>Tags:</span>
    @foreach($tag as $aTag)
    <a>{{$aTag["name"]}}</a><span class="comma" style="color: black">,</span>
    @endforeach
</div>
