<?php

function money_format($value) {
    return number_format($value, 0, '', ',');
}

function percent_format($value) {
    return number_format($value, 0);
}

function validate($value) {
    return isset($value) ? $value : "";
}
