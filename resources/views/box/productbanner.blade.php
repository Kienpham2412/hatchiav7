@include("box.validate")
<?php
    $productList;
?>

<div class="product-banner">
    @foreach($productList as $aProduct)
    <div class="col-sm-4">
        @include("box.productcontainer", ["aProduct"=>$aProduct])
    </div>
    @endforeach
</div>
<script language="javascript">
    function order(id){
        window.location = "/order/" + id;
    }
</script>
