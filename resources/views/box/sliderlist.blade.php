<style>
    .slick-slide img{
        display: inline-block;
    }
</style>

<?php
$sliderList;
$amount = count($sliderList);
?>

<div class="content flex-box slider-box">
    @foreach($sliderList as $aProduct)
        @include("box.productcontainer", ["aProduct"=>$aProduct])
    @endforeach
</div>

<script language="javascript">
    $(document).ready(function () {
        $(".slider-box").slick({
            infinite: true,
            dots: false,
            prevArrow: false,
            nextArrow: false,
            slidesToShow: 5,
            slidesToScroll: 5,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 4,
                    }
                }
            ]
        });
    });
</script>