
<?php
    $aProduct;
    $id = validate($aProduct["id"]);
    $flag = validate($aProduct["flag"]);
    $image = validate($aProduct['provider_reference']);
    $name = validate($aProduct["name"]);
    $title = validate($aProduct["title"]);
    $new = validate($aProduct["price_new"]);
    $old = validate($aProduct["price_old"]);
?>

<div class="product-container">
    <div class="image-box" style="background-image: url({{asset('images/thumb/'. $image)}})">
    <div class="flag-best-seller">
        {{$flag}}
    </div>
    </div>
    <div class="item-name">
        <a class="blue-link" href="/product/{{$id}}">{{$name}}</a>
        <p>{{$title}}</p>
    </div>
    <div class="item-price">
        <span class="new">{{money_format($new)}}</span>
        <span class="currency">VNĐ</span>
        <span class="old">{{money_format($old)}}</span>
    </div>
    <div class="item-order">
        <input class="btn btn-warning" type="button" id="order-{{$id}}" onclick="order({{$id}})" value="Đặt hàng">
    </div>
</div>

