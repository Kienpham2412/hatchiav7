<link type="text/css" rel="stylesheet" href="{{asset('css/productinfo.css')}}"/>
@include("box.validate")
<?php
    $id = validate($product[0]["id"]);
    $image = validate($product[0]["provider_reference"]);
    $name = validate($product[0]["name"]);
    $old = validate($product[0]["price_old"]);
    $new = validate($product[0]["price_new"]);
    $sale = $old - $new;
    $percent = ($sale/$old)*100;
?>

<div class="product-info">
    <div class="col-sm-4">
        <img src="{{asset('images/thumb/'. $image)}}" alt="image"/>
    </div>
    <div class="col-sm-8">
        <div class="name">
            <h2 class="black-title-22">{{$name}}</h2>
        </div>
        <div class="price">
            <p class="black-title">Giá thị trường: <span class="old">{{money_format($old)}}</span> đ</p>
            <p class="red-title">{{money_format($new)}} đ</p>
            <p class="black-title">Tiết kiệm: {{money_format($sale)}} đ ({{percent_format($percent)}}%)</p>
        </div>
        <div class="order">
            <p class="black-title">Tình trạng: còn hàng</p>
            <input class="btn btn-warning" type="button" id="order-{{$id}}" onclick="order({{$id}})" value="Đặt hàng">
        </div>
    </div>
</div>
<script language="javascript">
    function order(id){
        window.location = "/order/" + id;
    }
</script>
