<link type="text/css" rel="stylesheet" href="{{asset('css/feedback.css')}}"/>

<?php
    $feedback;
    $amount = count($feedback);
?>

<div class="feedback">
    <h2>Phản hồi</h2>
    <div class="comment-list">
        @for($i=0;$i<$amount;$i++)
        <div class="comment-box flex-box">
            <div class="avatar-box">
                <img src="{{asset('images/avatar.png')}}"/>
            </div>
            <div class="name-and-comment">
                <div >
                    <p>{{$feedback[$i]["full_name"]}}</p>
                    <span>{{$feedback[$i]["created_at"]}}</span>
                </div>
                <div class="comment">
                    <p>{{$feedback[$i]["content"]}}</p>
                </div>
            </div>
        </div>
        @endfor
    </div>
</div>
