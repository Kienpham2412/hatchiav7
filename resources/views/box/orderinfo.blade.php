<link type="text/css" rel="stylesheet" href="{{asset('css/postbox.css')}}"/>

<?php
//    $price = isset($product[0]["price_new"]) ? $product[0]["price_new"] : "";
    $price = validate($product[0]["price_new"]);
?>

@include("box.upperbanner", ["title"=>"THÔNG TIN ĐẶT HÀNG"])
<div class="order-form content">
    <div class="row">
        <div class="col-md-6 post-box">
            <div>
                <label>Họ và tên</label>
                <input type="text" id="name" class="form-control">
            </div>
        </div>
        <div class="col-md-6 post-box">
            <div>
                <label>Số điện thoại</label>
                <input type="text" id="name" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 post-box">
            <div>
                <label>Địa chỉ nhận hàng</label>
                <textarea id="name" class="form-control"></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 post-box">
            <div>
                <label>Số lượng</label>
                <select id="order-number" class="form-control">
                    @for($i=1;$i<=10;$i++)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                </select>
                <p class="black-title" style="margin-top: 10px">Tổng cộng: <label id="total-price" class="black-title">{{money_format($price)}} đ</label></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <input type="button" class="btn btn-primary" value="hoàn thành" id="order">
        </div>
    </div>
</div>

<script language="javascript">
    function numberWithCommas(x) {
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1,$2");
        return x;
    }

    var price = {{$price}};
    var totalPrice;
    $("#order-number").change(function(){
        var amount = $("#order-number").val();
        totalPrice = price*amount;
        $("#total-price").html(numberWithCommas(totalPrice) + " đ");
    });
</script>
