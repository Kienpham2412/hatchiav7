<link type="text/css" rel="stylesheet" href="{{asset('css/handbook.css')}}"/>

<?php
    $article;
    $amount = count($article);
?>

<div class="handbook">
    @include("box.upperbanner", ["title"=>"CẨM NANG HẠT CHIA"])
    <div class="content">
        <img src="{{asset('images/10 ways to eat chia.png')}}"/>
        <a class="blue-link" style="font-weight: bold">10 cách sử dụng hạt CHIA hiệu quả nhất</a>
        <ul>
            @for($i=0;$i<$amount;$i++)
            <?php
                $id = validate($article[$i]["id"]);
                $name = validate($article[$i]["name"]);
            ?>
            <li>
                <a class="blue-link" href="/tin-tuc/{{$id}}">
                    <strong>{{$i + 1}}</strong>
                    <span>{{$name}}</span>
                </a>
            </li>
            @endfor
        </ul>
    </div>
</div>
