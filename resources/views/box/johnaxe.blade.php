<link type="text/css" rel="stylesheet" href="{{asset('css/johnaxe.css')}}"/>

<div class="john-axe content">
    <img src="{{asset('images/96x96xjosh-axe.png')}}">
    <div class="info">
        <a>Tiến sĩ John Axe</a>
        <a>https://hatchiamynhapkhau.com/bac-si-josh-axe</a>
        <p>Bác sĩ dinh dưỡng nổi tiếng Người Mỹ, với niềm đam mê giúp mọi người
            khỏe mạnh. Có nghiên cứu đặc biệt chuyên sâu về hạt chia.</p>
    </div>
</div>
