<link type="text/css" rel="stylesheet" href="{{asset('css/sitenavi.css')}}"/>

<?php
    $flag = isset($product[0]["flag"]) ? $product[0]["flag"] : "";
    $name = isset($product[0]["name"]) ? $product[0]["name"] : "";
?>

<div class="site-navi">
    <a class="blue-link" href="/">Trang chủ</a>
    @if(isset($alt_flag))
        <span class="blue-link">/</span>
        <span class="blue-link">{{$alt_flag}}</span>
    @endif
    @if(isset($product[0]["flag"]))
        <span class="blue-link">/</span>
        <span class="blue-link">{{$flag}}</span>
    @endif
    @if(isset($product[0]["name"]))
        <span class="blue-link">/</span>
        <span class="blue-link">{{$name}}</span>
    @endif
</div>
