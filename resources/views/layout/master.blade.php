<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" data-ca-mode="free"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
        <meta content="telephone=no" name="format-detection"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta property="og:site_name" content="Hạt Chia Là Gì, Có Công Dụng Thế Nào, Mua Ở Đâu"/>
        <meta property="og:url" content="http://hatchiamynhapkhau.com/"/>
        <meta property="og:description" content="Hạt Chia Mỹ - Chia seed Mỹ, địa chỉ mua hạt chia chính hãng ở tại Hà Nội, HCM"/>
        <meta property="og:title" content="Hạt Chia Mỹ, Hạt chia Nutiva, Hạt Chia Nam Mỹ"/>
        <meta name="geo.region" content="VN"/>
        <meta http-equiv="content-language" content="vi-en"/>
        <meta name="robots" content="noodp,index,follow"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/productcontainer.css')}}"/>
        
        <link type="text/css" rel="stylesheet" href="{{asset('fontawesome-free-5.15.3-web/css/all.css')}}"/>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <meta name="apple-mobile-web-app-capable" content="yes"/>
        <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('favicon.ico')}}"/>
        <title>Hạt chia Mỹ</title>
        <meta name="google-site-verification" content="sz6uxcliHj5RKZyPp9pVtDUJqGkDm1IrZ6Xl38gdVZE"/>
    </head>
    <body>
        <div class="wrapper">
            <div class="header">
                <nav class="navbar navbar-default">
                    <div class="wrapper" >
                        <div class="container-fluid  nav-header">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a style="padding: 4px" class="navbar-brand" href="/">
                                    <img style="margin-bottom: 3px; width: 180px" src="{{asset('images/logo.png')}}" title="hat chia my" alt="hat chia my">
                                </a>
                            </div>
                            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a class="category" href="/">Hạt chia Mỹ</a></li>
                                    <li><a class="category" href="/hat-chia-uc">Hạt chia Úc</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>

            @yield("content")

            <div class="footer">
                <div class="upper-border"></div>
                <div class="content container">
                    <div class="col-md-4">
                        <span class="menu">HỖ TRỢ KHÁCH HÀNG</span>
                        <ul>
                            <li class="ty-footer-menu__item">
                                <a href="">Chính sách vận chuyển</a>
                            </li>
                            <li class="ty-footer-menu__item">
                                <a href="">Chính sách đổi trả hàng</a>
                            </li>
                            <li class="ty-footer-menu__item">
                                <a href="">Chính sách thanh toán</a>
                            </li>
                        </ul>
                    </div>

                    <div class="footer-menu col-md-4">
                        <span class="menu">LIÊN HỆ MUA HÀNG</span>
                        <ul class="">
                            <li class="item">Mua hạt chia ở Hà Nội/HCM</li>
                            <li class="item menu-contact">
                                <ul class=""><li>0906 280 315</li></ul>
                            </li>
                            <li class="item">Mua hạt chia ở Hải Phòng:</li>
                            <li class="item menu-contact">
                                <ul><li>0394 814 822</li></ul>
                            </li>
                        </ul>
                    </div>

                    <div class="footer-menu col-md-4">
                        <span class="menu">CẨM NANG HẠT CHIA</span>
                        <ul>
                            <li class="item">Công dụng của hạt chia Mỹ</li>
                            <li class="item"><a href="">Cách dùng hạt chia Mỹ</a></li>
                            <li class="item">Mua hạt chia Mỹ ở đâu</li>
                            <li class="item">Công dụng của hạt chia Mỹ cho bà bầu</li>
                            <li class="item">Hạt chia Mỹ chữa tiểu đường</li>
                        </ul>
                    </div>

                    <div class="callnow">
                        <a href="tel:0906280315">
                            <span class="fas fa-phone-square-alt fa-lg"></span>
                            <span>0906.280.315</span>
                        </a>
                    </div>

                    <div class="dmca">
                        <a href="//www.dmca.com/Protection/Status.aspx?ID=f3f781b1-a1d6-4e43-88fb-3bfa4bbf715f" title="DMCA.com Protection Status" class="dmca-badge">
                            <img src="https://images.dmca.com/Badges/dmca_protected_sml_120m.png?ID=f3f781b1-a1d6-4e43-88fb-3bfa4bbf715f" alt="DMCA.com Protection Status" data-pagespeed-url-hash="591162408" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"/>
                        </a>  
                        <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"></script>
                    </div>

                    <div class="support-icon-right">
                        <div style="text-align: right;margin-right: 5px;">
                            <a href="https://zalo.me/0906280315">
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAAA2CAMAAAC7m5rvAAACW1BMVEUBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcBgMcCgMcCgccDgccEgcgEgsgFgsgGgsgGg8gHg8gIhMkJhMkKhMkLhckMhckNhsoOhsoPh8oQh8oQiMoRiMoSicsTicsUicsUissXi8wYi8wYjMwZjMwajMwcjc0djs0gj84ikM4kks8mk88nk88ok9AqlNAqldAsltEvl9ExmNIymNIymdI0mdI1mtI2mtM2m9M3m9M4nNM8ndQ/n9VAoNVCoNVHo9ZIpNdJpNdLpddMpddNpthOp9hPp9hSqNlTqdlUqtlVqtlWqtpXq9pYq9pYrNpZrNparNtdrtter9xfr9xhsNxisdxjsd1lst1mst1ms91ns91ns95qtd5rtd5ttt9utt9wt99wuOBxuOB0uuB2uuF2u+F3u+F8vuJ9vuJ/v+OCwOOCweOCweSGwuSIw+WJxOWMxeaOxuaRyOeTyeeUyuiVyueVyuiWy+iXy+iazembzemczemczumdzumfz+qm0+uo1Oyr1eys1e2t1u2y2e602e622u+22++43O+63fC+3vHA3/HB4PHE4vLH4/PK5fPO5vTP5/XQ5/XR6PXS6fXT6fXU6fbU6vbY7Pbc7ffd7vfd7vje7vjg7/ji8fnk8fnp9Prq9frs9fvs9vvu9vvu9/vv9/vx+Pzy+Pzy+fzz+fz0+f31+v32+/33+/34/P77/f78/v79/v79/v/+/v/+//////9igdYBAAAAGnRSTlMAAQcICgwNFThHWnh5gISpsLO61dbp8PX2+9fstNEAAALfSURBVEjHnZb3W9NAGMfDprullJYrKqC4QBkCbnAjoqioiIK0OMEJqDjAhbhRq+Leew9wL5RK4P2zvCRtyV3aNE++P7295vPk8m6GkSpGYzBbHQjLYTUbNDGMAkVrLXZEyG7RRoeB4ow2FEQ2Y5wMFKVLQiGUpIsKRcUnIhklxgeFIvTJSFbJ+ggpFWlCYWWKpKnYBKRACbHUuxRRmCPeF2FCCmUSf58eKZZe5Hm/D8csddVxcmWG9mcgDlH+eG3qAV7dlXLx88dd5zuoYQXqXoHsNXW+PPRlVMorgbo9Vv7rkoT8NPp+FgrU88xwXjHyleLP+SKe+jMnrDNtXB1pEYEdUhAELcYsBPY1SwFmwR3ATmAn6EdS86SYPYbRIAJbho0ZB9sOtx2YJpwe/Z0v5TSMgcD6xmOjiWVxBPcJpx6YKcUMjJnA3qVgwzkxZ5X3bXbu8VsNx5ZgbPT+a53zCczMWAnsoWAWfOhdOOoxfO6HTg8UXYFv3t4SMWZlHAT2gLfS7wxsQSvhclojXPRABXs3Yw14xJiDQQT2krfa4YwTbYBGtIDD6qEdTRq8SdySwr6PxMZa9ueeXTvXDzxbfhUueKC8r7vyLByRw2AeNk7x6XLp3CD8YjtOeqe2/AN4kiuLtWIjo6SsrKx0grO8fkppVs4ihOZuq8ogI0C5BN6nK+kMDioAANuVYFYq3AAfCxVgZiq5uOJOC48Z6FTGOj88+LP52aJUpgqH09ag1PQXxaLCocqUU10wqqoHikVlSjcFgK4RUmhWFy6lYnFToFoQ24Hzyzm7uXVxqv+pcdXXvdxfReIWFGh4k3ns6e4dLZ7XnPXpRntzfcPe0/f/Crdg88QNL9Beh70BWT0i22ugmW8clKP6V5PNPDA6nE0/QlNfNtOjY2hQZVesc7tra6prh+Ryu911bteKTMmgUjsW1Q5hlSNf7YKhdp1RuzypXdVUL4Zq11DVS6/SFfs/Mp76dE0RYakAAAAASUVORK5CYII=" style="width:54px;margin-bottom:4px;" data-pagespeed-url-hash="3031108032" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
