@extends("layout.master")

@section("content")
<link type="text/css" rel="stylesheet" href="{{asset('css/hatchia.css')}}"/>

<div class="container">
    <div class="col-md-8">
        <div class="row site-box">
            <div class="col-md-4" style="padding-left: 0">
            @include("box.sitenavi", ["alt_flag"=>$flag])
            </div>
            <div class="col-md-4" style="padding: 0">
            <div class="line"></div>
            </div>
            <div class="col-md-4">
            @include("box.likeshare")
            </div>
        </div>
        <div class="row content" style="margin-bottom: 15px;">
            @include("box.productbanner2", ["productList"=>$productList])
        </div>
        <div class="row">
            @include("box.articles", ["article"=>$article])
        </div>
        <div class="row">
            @include("box.johnaxe")
        </div>
    </div>
    <div class="col-md-4">
        <div class="right-menu">
        @include("box.handbook", ["article"=>$article])
        @include("box.chiaseed")
        @include("box.certification")
        </div>
    </div>
</div>
@endsection
