@extends("layout.master")

@section("content")
<div class="container">
    <div class="row content">
        @include("box.productbanner", ["productList" => $productList])
    </div>
    <div class="row">
        @include("box.news", ["article"=>$article])
    </div>
    <div class="row">
        @include("box.tag", ["tag"=>$tag])
        @include("box.johnaxe")
    </div>
</div>
@endsection