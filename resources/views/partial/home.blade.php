@extends("layout.master")

@section("content")
<link type='text/css' rel="stylesheet" href="{{asset('css/home.css')}}"/>
<div class="container">
    <div class="row">
        @include("box.upperbanner", ["title"=>"Mua hạt chia ở Hà Nội - HCM"])
        <img style="width: 100%" src="{{asset('images/home-banner.jpg')}}"/>
    </div>

    <div class="row content">
        @include("box.productbanner", ["productList" => $productList])
    </div>

    <div class="row">
        @include("box.upperbanner", ["title"=>"HẠT CHIA GÓI NHỎ"])
        @include("box.sliderlist", ["sliderList"=>$sliderList])
    </div>

    <div class="row">
        @include("box.upperbanner", ["title"=>"HẠT CHIA LÀ GÌ"])
        <div class="col-md-8 content">
            <div class="">
                @include("box.likeshare")
                <div class="line"></div>
                @include("box.homecontent", ["content"=>$content])
                @include("box.tag", ["tag"=>$tag])
                @include("box.johnaxe")
                @include("box.feedback", ["feedback"=>$feedback])
                @include("box.postbox")
            </div>
        </div>
        <div class="col-md-4">
            <div class="right-menu">
                @include("box.handbook", ["article"=>$article])
                @include("box.chiaseed", ["productList"=>$productList])
                @include("box.certification")
            </div>
        </div>
    </div>
</div>

@endsection
