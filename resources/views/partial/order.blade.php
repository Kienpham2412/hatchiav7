<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
@extends("layout.master")

@section("content")
<div class="container">
    <div class="row">
        <div class="col-md-8" style="padding-left: 0">
            @include("box.productinfo", ["product"=>$product]) 
        </div>
    </div>
    <div class="row" style="margin-top: 15px">
        <div class="col-md-8">
            @include("box.orderinfo", ["product"=>$product])
        </div>
        <div class="col-md-4">
            @include("box.handbook", ["article"=>$article])
        </div>
    </div>
</div>
@endsection
