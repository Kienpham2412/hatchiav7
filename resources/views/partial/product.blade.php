@extends("layout.master")

@section("content")
<div class="container">
    @include("box.sitenavi", ["product"=>$product])
    <div class="col-md-8">
        <div class="row">
            @include("box.productinfo", ["product"=>$product])  
        </div>
        <div class="row">
            <h2 class="black-title-22">Thông tin sản phẩm</h2>
        </div>
        <div class="row">
            @include("box.johnaxe")
        </div>
    </div>
    <div class="col-md-4">
        @include("box.handbook", ["article"=>$article])
    </div>
</div>
@endsection