<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;
use App\Http\Controllers\FrontController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, "home"]);
Route::get('/hat-chia-uc', [FrontController::class, "hatChia"]);
Route::get('/product/{id}', [FrontController::class, "product"]);
Route::get('/order/{id}', [FrontController::class, "order"]);
Route::get('/tin-tuc/{id}', [FrontController::class, "news"]);

Route::post('/data/getProducts', [DataController::class, "getProducts"]);
